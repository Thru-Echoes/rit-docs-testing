---
title: Savio User Guide
keywords: high performance computing
last_updated: October 17, 2019
tags: [hpc]
sidebar: hpc_sidebar
permalink: services/high-performance-computing/user-guide/hardware-config
folder: hpc
# DO NOT EDIT THE PREAMBLE BELOW THIS LINE #
search_content: Savio Hardware Configuration The following table details the hardware configuration of each partition of nodes  Each partition corresponds to a combination of a generation and type of node  Partition Nodes Node List CPU Model   Cores Node Memory Node Infiniband Specialty Scheduler Allocation savio 164 n0 000 095 savio1 n0 100 167 savio1 Intel Xeon E5 2670 v2 20 64 GB FDR   By Node savio bigmem 4 n0 096 099 savio1 Intel Xeon E5 2670 v2 20 512 GB FDR BIGMEM By Node savio2 124 n0 027 150 savio2 Intel Xeon E5 2670 v3 24 64 GB FDR   By Node savio2 4 n0 290 293 savio2 Intel Xeon E5 2650 v4 24 64 GB FDR   By Node savio2 35 n0 187 210 savio2 n0 230 240 savio2 Intel Xeon E5 2680 v4 28 64 GB FDR   By Node savio2 bigmem 36 n0 151 182 savio2 Intel Xeon E5 2670 v3 24 128 GB FDR BIGMEM By Node savio2 bigmem 8 n0 282 289 savio2 Intel Xeon E5 2650 v3 24 128 GB FDR BIGMEM By Node savio2 htc 20 n0 000 011 savio2 n0 215 222 savio2 Intel Xeon E5 2643 v3 12 128 GB FDR HTC By Core savio2 gpu 17 n0 012 026 savio2 n0 223 224 savio2 Intel Xeon E5 2623 v3 8 64 GB FDR 4x Nvidia K80 By Core savio2 1080ti 7 n0 227 229 savio2 n0 298 301  Intel Xeon E5 2623 v3 8 64 GB FDR 4x Nvidia 1080ti By Core savio2 knl 28 n0 254 281 savio2 Intel Xeon Phi 7210 64 188 GB FDR Intel Phi By Node savio3 108 n0 010 029 savio3 n0 042 129 savio3 Intel Xeon Skylake 6130   2 1 GHz 32 96 GB FDR  By Node savio3 8 n0 130 133 savio3 n0 139 142 savio3 Intel Xeon Skylake 6230   2 1 GHz 40 96 GB FDR  By Node savio3 bigmem 16 n0 006 009 savio3 n0 030 041 savio3 Intel Xeon Skylake 6130   2 1 GHz 32 384 GB FDR BIGMEM By Node savio3 xlmem 2 n0 000 001 savio3 Intel Xeon Skylake 6130   2 1 GHz 32 1 5 TB FDR XL Memory By Node savio3 2080ti 5 n0 134 138 savio3 Intel Xeon Skylake 6130   2 1 GHz 8 96 GB FDR 4x GTX 2080ti GPU By Core savio3 2080ti 3 n0 143 145 savio3 Intel Xeon Skylake 6130   2 1 GHz 32 384 GB FDR 8x GTX 2080ti GPU By Core savio3 gpu 1 n0005 savio3 Intel Xeon Skylake 6130   2 1 GHz 8 96 GB FDR 2x Tesla V100 GPU By Core CGRL Hardware Configuration The CGRL hardware consists of the Vector cluster  a cluster separate from Savio  and the Rosalind condo within Savio  Vector and Rosalind are heterogeneous  with a mix of several different types of nodes  Please be aware of these various hardware configurations  along with their associated scheduler configurations   when specifying options for running your jobs  Cluster Nodes Node List CPU Cores Node Memory Node Scheduler Allocation Vector 11 n00 00 03 vector0 Intel Xeon X5650  2 66 GHz 12 96 GB By Core n0004 vector0 AMD Opteron 6176  2 3 GHz 48 256 GB By Core n00 05 08 vector0 Intel Xeon E5 2670  2 60 GHz 16 128 GB By Core n00 09 n00 10 vector0 Intel Xeon X5650  2 66 GHz 12 48 GB By Core Rosalind  Savio1  8 floating condo within  n0 000 095 savio1  n0 100 167 savio1 Intel Xeon E5 2670 v2  2 50 GHz 20 64 GB By Node Rosalind  Savio2 HTC  8 floating condo within  n0 000 011 savio2  n0 215 222 savio2 Intel Xeon E5 2643 v3  3 40 GHz 12 128 GB By Core
---

<h3><a id="Savio-Hardware">Savio Hardware Configuration</a></h3>
<p>The following table details the hardware configuration of each partition of nodes. Each partition corresponds to a combination of a generation and type of node. </p>
<table id="hardware-config-table" align="center" border="1" cellspacing="0"><tbody><tr><th style="text-align:center">Partition</th>
<th style="text-align:center">Nodes</th>
<th style="text-align:center">Node List</th>
<th style="text-align:center">CPU Model</th>
<th style="text-align:center"># Cores/Node</th>
<th style="text-align:center">Memory/Node</th>
<th style="text-align:center">Infiniband</th>
<th style="text-align:center">Specialty</th>
<th style="text-align:center">Scheduler Allocation</th>
</tr><tr><td style="text-align:center; vertical-align:middle">savio</td>
<td style="text-align:center; vertical-align:middle">164</td>
<td style="text-align:center; vertical-align:middle">n0[000-095].savio1<br>
n0[100-167].savio1</td>
<td style="text-align:center; vertical-align:middle">Intel Xeon E5-2670 v2</td>
<td style="text-align:center; vertical-align:middle">20</td>
<td style="text-align:center; vertical-align:middle">64 GB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle">-</td>
<td style="text-align:center; vertical-align:middle">By Node</td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio_bigmem</td>
<td style="text-align:center; vertical-align:middle">4</td>
<td style="text-align:center; vertical-align:middle">n0[096-099].savio1</td>
<td style="text-align:center; vertical-align:middle">Intel Xeon E5-2670 v2</td>
<td style="text-align:center; vertical-align:middle">20</td>
<td style="text-align:center; vertical-align:middle">512 GB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle">BIGMEM</td>
<td style="text-align:center; vertical-align:middle">By Node</td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio2</td>
<td style="text-align:center; vertical-align:middle">124</td>
<td style="text-align:center; vertical-align:middle">n0[027-150].savio2</td>
<td style="text-align:center; vertical-align:middle">Intel Xeon E5-2670 v3</td>
<td style="text-align:center; vertical-align:middle">24</td>
<td style="text-align:center; vertical-align:middle">64 GB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle">-</td>
<td style="text-align:center; vertical-align:middle">By Node</td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio2</td>
<td style="text-align:center; vertical-align:middle">4</td>
<td style="text-align:center; vertical-align:middle">n0[290-293].savio2</td>
<td style="text-align:center; vertical-align:middle">Intel Xeon E5-2650 v4</td>
<td style="text-align:center; vertical-align:middle">24</td>
<td style="text-align:center; vertical-align:middle">64 GB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle">-</td>
<td style="text-align:center; vertical-align:middle">By Node</td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio2</td>
<td style="text-align:center; vertical-align:middle">35</td>
<td style="text-align:center; vertical-align:middle">n0[187-210].savio2<br>
n0[230-240].savio2</td>
<td style="text-align:center; vertical-align:middle">Intel Xeon E5-2680 v4</td>
<td style="text-align:center; vertical-align:middle">28</td>
<td style="text-align:center; vertical-align:middle">64 GB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle">-</td>
<td style="text-align:center; vertical-align:middle">By Node</td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio2_bigmem</td>
<td style="text-align:center; vertical-align:middle">36</td>
<td style="text-align:center; vertical-align:middle">n0[151-182].savio2</td>
<td style="text-align:center; vertical-align:middle">Intel Xeon E5-2670 v3</td>
<td style="text-align:center; vertical-align:middle">24</td>
<td style="text-align:center; vertical-align:middle">128 GB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle">BIGMEM</td>
<td style="text-align:center; vertical-align:middle">By Node</td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio2_bigmem</td>
<td style="text-align:center; vertical-align:middle">8</td>
<td style="text-align:center; vertical-align:middle">n0[282-289].savio2</td>
<td style="text-align:center; vertical-align:middle">Intel Xeon E5-2650 v3</td>
<td style="text-align:center; vertical-align:middle">24</td>
<td style="text-align:center; vertical-align:middle">128 GB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle">BIGMEM</td>
<td style="text-align:center; vertical-align:middle">By Node</td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio2_htc</td>
<td style="text-align:center; vertical-align:middle">20</td>
<td style="text-align:center; vertical-align:middle">n0[000-011].savio2<br>
n0[215-222].savio2</td>
<td style="text-align:center; vertical-align:middle">Intel Xeon E5-2643 v3</td>
<td style="text-align:center; vertical-align:middle">12</td>
<td style="text-align:center; vertical-align:middle">128 GB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle">HTC</td>
<td style="text-align:center; vertical-align:middle">By Core</td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio2_gpu</td>
<td style="text-align:center; vertical-align:middle">17</td>
<td style="text-align:center; vertical-align:middle">n0[012-026].savio2<br>
n0[223-224].savio2</td>
<td style="text-align:center; vertical-align:middle">Intel Xeon E5-2623 v3</td>
<td style="text-align:center; vertical-align:middle">8</td>
<td style="text-align:center; vertical-align:middle">64 GB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle">4x Nvidia K80</td>
<td style="text-align:center; vertical-align:middle">By Core</td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio2_1080ti</td>
<td style="text-align:center; vertical-align:middle">7</td>
<td style="text-align:center; vertical-align:middle">n0[227-229].savio2<br>n0[298-301]</td>
<td style="text-align:center; vertical-align:middle">Intel Xeon E5-2623 v3</td>
<td style="text-align:center; vertical-align:middle">8</td>
<td style="text-align:center; vertical-align:middle">64 GB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle">4x Nvidia 1080ti</td>
<td style="text-align:center; vertical-align:middle">By Core</td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio2_knl</td>
<td style="text-align:center; vertical-align:middle">28</td>
<td style="text-align:center; vertical-align:middle">n0[254-281].savio2</td>
<td style="text-align:center; vertical-align:middle">Intel Xeon Phi 7210</td>
<td style="text-align:center; vertical-align:middle">64</td>
<td style="text-align:center; vertical-align:middle">188 GB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle">Intel Phi</td>
<td style="text-align:center; vertical-align:middle">By Node</td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio3</td>
<td style="text-align:center; vertical-align:middle">108</td>
<td style="text-align:center; vertical-align:middle">n0[010-029].savio3<br>n0[042-129].savio3</td>
<td style="text-align:center; vertical-align:middle">Intel Xeon Skylake 6130 @ 2.1 GHz</td>
<td style="text-align:center; vertical-align:middle">32</td>
<td style="text-align:center; vertical-align:middle">96 GB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle">&nbsp;</td>
<td style="text-align:center; vertical-align:middle">By Node</td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio3</td>
<td style="text-align:center; vertical-align:middle">8</td>
<td style="text-align:center; vertical-align:middle">n0[130-133].savio3<br>n0[139-142].savio3</td>
<td style="text-align:center; vertical-align:middle">Intel Xeon Skylake 6230 @ 2.1 GHz</td>
<td style="text-align:center; vertical-align:middle">40</td>
<td style="text-align:center; vertical-align:middle">96 GB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle">&nbsp;</td>
<td style="text-align:center; vertical-align:middle">By Node</td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio3_bigmem</td>
<td style="text-align:center; vertical-align:middle">16</td>
<td style="text-align:center; vertical-align:middle">n0[006-009].savio3<br>n0[030-041].savio3</td>
<td style="text-align:center; vertical-align:middle">Intel Xeon Skylake 6130 @ 2.1 GHz</td>
<td style="text-align:center; vertical-align:middle">32</td>
<td style="text-align:center; vertical-align:middle">384 GB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle">BIGMEM</td>
<td style="text-align:center; vertical-align:middle">By Node</td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio3_xlmem</td>
<td style="text-align:center; vertical-align:middle">2</td>
<td style="text-align:center; vertical-align:middle">n0[000-001].savio3</td>
<td style="text-align:center; vertical-align:middle">Intel Xeon Skylake 6130 @ 2.1 GHz</td>
<td style="text-align:center; vertical-align:middle">32</td>
<td style="text-align:center; vertical-align:middle">1.5 TB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle">XL Memory</td>
<td style="text-align:center; vertical-align:middle">By Node</td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio3_2080ti</td>
<td style="text-align:center; vertical-align:middle">5</td>
<td style="text-align:center; vertical-align:middle">n0[134-138].savio3</td>
<td style="text-align:center; vertical-align:middle">Intel Xeon Skylake 6130 @ 2.1 GHz</td>
<td style="text-align:center; vertical-align:middle">8</td>
<td style="text-align:center; vertical-align:middle">96 GB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle">4x GTX 2080ti GPU</td>
<td style="text-align:center; vertical-align:middle">By Core</td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio3_2080ti</td>
<td style="text-align:center; vertical-align:middle">3</td>
<td style="text-align:center; vertical-align:middle">n0[143-145].savio3</td>
<td style="text-align:center; vertical-align:middle">Intel Xeon Skylake 6130 @ 2.1 GHz</td>
<td style="text-align:center; vertical-align:middle">32</td>
<td style="text-align:center; vertical-align:middle">384 GB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle">8x GTX 2080ti GPU</td>
<td style="text-align:center; vertical-align:middle">By Core</td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio3_gpu</td>
<td style="text-align:center; vertical-align:middle">1</td>
<td style="text-align:center; vertical-align:middle">n0005.savio3</td>
<td style="text-align:center; vertical-align:middle">Intel Xeon Skylake 6130 @ 2.1 GHz</td>
<td style="text-align:center; vertical-align:middle">8</td>
<td style="text-align:center; vertical-align:middle">96 GB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle">2x Tesla V100 GPU</td>
<td style="text-align:center; vertical-align:middle">By Core</td>
</tr></tbody></table>

<h3><a id="CGRL-hardware">CGRL Hardware Configuration</a></h3>
<p>The CGRL hardware consists of the Vector cluster (a cluster separate from Savio) and the Rosalind condo within Savio. Vector and Rosalind are heterogeneous, with a mix of several different types of nodes. Please be aware of these various hardware configurations, along with their associated <a href="#Scheduler-Configuration" class="toc-filter-processed">scheduler configurations</a>, when specifying options for running your jobs.</p>
<table border="1" align="center"><tbody><tr style="background-color:#D3D3D3"><th>Cluster</th>
<th>Nodes</th>
<th>Node List</th>
<th>CPU</th>
<th>Cores/Node</th>
<th>Memory/Node</th>
<th>Scheduler Allocation</th>
</tr><tr><td rowspan="4">Vector</td>
<td rowspan="4">11</td>
<td>n00[00-03].vector0</td>
<td>Intel Xeon X5650, 2.66 GHz</td>
<td>12</td>
<td>96 GB</td>
<td>By Core</td>
</tr><tr><td>n0004.vector0</td>
<td>AMD Opteron 6176, 2.3 GHz</td>
<td>48</td>
<td>256 GB</td>
<td>By Core</td>
</tr><tr><td>n00[05-08].vector0</td>
<td>Intel Xeon E5-2670, 2.60 GHz</td>
<td>16</td>
<td>128 GB</td>
<td>By Core</td>
</tr><tr><td>n00[09]-n00[10].vector0</td>
<td>Intel Xeon X5650, 2.66 GHz</td>
<td>12</td>
<td>48 GB</td>
<td>By Core</td>
</tr><tr><td>Rosalind (Savio1)</td>
<td>8</td>
<td>floating condo within:&nbsp;&nbsp;&nbsp;&nbsp;
<p>n0[000-095].savio1, n0[100-167].savio1</p></td>
<td>Intel Xeon E5-2670 v2, 2.50 GHz</td>
<td>20</td>
<td>64 GB</td>
<td>By Node</td>
</tr><tr><td>Rosalind (Savio2 HTC)</td>
<td>8</td>
<td>floating condo within:&nbsp;&nbsp;&nbsp;&nbsp;
<p>n0[000-011].savio2, n0[215-222].savio2</p></td>
<td>Intel Xeon E5-2643 v3, 3.40 GHz</td>
<td>12</td>
<td>128 GB</td>
<td>By Core</td>
</tr></tbody></table>
