import json
import unicodedata
import glob
from html.parser import HTMLParser
import re

import sys, os, inspect
from os.path import join as oj
current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parent_dir = os.path.dirname(current_dir)

# see https://stackoverflow.com/questions/753052/strip-html-from-strings-in-python
class MLStripper(HTMLParser):
    def __init__(self):
        super().__init__()
        self.reset()
        self.strict = False
        self.convert_charrefs = True
        self.fed = []
    def handle_data(self, d):
        self.fed.append(d)
    def get_data(self):
        return self.fed

def strip_tags(html):
    s = MLStripper()
    s.feed(html)
    return s.get_data()

if __name__ == "__main__":
    pages_dir = oj(
        parent_dir,
        'pages',
        'services',
        'high-performance-computing'
    )

    # Get the list of all pages
    pages_list = glob.glob(pages_dir + '/**/*.md', recursive=True)

    for path in pages_list:
        print(path)

        with open(path, 'r+') as f:
            page = f.read()

            # find correct spot in preamble to add content
            page_lines = page.splitlines()
            end_preamble = list(filter(
                lambda x: x not in set([0, -1]),
                [i if '---' in line else -1 for (i, line) in enumerate(page_lines)]
            ))[0]

            # remove html tags
            no_tags = [strip_tags(line) for line in page_lines[(end_preamble+1):]]
            no_tags_flat = [item for sublist in no_tags for item in sublist]

            cleaned = ' '.join([re.sub(r'[^A-Za-z0-9 ]+', ' ', chunk.strip()) for chunk in no_tags_flat])
            content_exists = 'search_content:' in page_lines[end_preamble - 1]

            if content_exists:
                page_lines[end_preamble - 1] = 'search_content: ' + cleaned
            else:
                page_lines.insert(end_preamble, '# DO NOT EDIT THE PREAMBLE BELOW THIS LINE #')
                page_lines.insert(end_preamble + 1, 'search_content: ' + cleaned)

            # overwrite the existing file
            f.seek(0)
            for line in page_lines:
                f.write(line + '\n')
            # truncate the file's size at the current position
            f.truncate()
